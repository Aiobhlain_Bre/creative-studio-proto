let canvas = document.getElementById('canvas-clock-hour');
let ctx = canvas.getContext('2d');

let date = new Date();
let hour = date.getHours();
let min = date.getMinutes();
let sec = date.getSeconds();



//canvas.width = 500;
//canvas.height = 500;

function drawHour(delta) {
    requestAnimationFrame(drawHour);
    canvas.width = canvas.width;
    ctx.fillStyle = "rgba(0, 0, 0, 0.4)";
    
    
    let randomLeft = (Math.abs(Math.pow( Math.sin(delta/(hour * 3600)), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/(hour * 3600)) + 10), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/(hour * 3600))+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/(hour * 3600))+1), 2)) * 100;
    
    ctx.beginPath();
    ctx.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctx.bezierCurveTo(canvas.width / 3, randomLeftConstraint, canvas.width / 3 * 2, randomRightConstraint, canvas.width, randomRight);


    ctx.stroke();
    //ctx.fill();
}
requestAnimationFrame(drawHour);

let canvasTwo = document.getElementById('canvas-clock-min');
let ctxTwo = canvas.getContext('2d');

//canvasTwo.width = 500;
//canvasTwo.height = 500;

function drawMin(delta) {
    requestAnimationFrame(drawMin);
    canvasTwo.width = canvas.width;
    ctxTwo.fillStyle = "rgba(0, 0, 0, 0.4)";
    
    
    let randomLeft = (Math.abs(Math.pow( Math.sin(delta/(min * 300)), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/(min * 300)) + 6), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/(min * 300))+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/(min * 300))+1), 2)) * 100;
    
    ctxTwo.beginPath();
    ctxTwo.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxTwo.bezierCurveTo(canvasTwo.width / 3, randomLeftConstraint, canvasTwo.width / 3 * 2, randomRightConstraint, canvasTwo.width, randomRight);


    ctxTwo.stroke();
}
requestAnimationFrame(drawMin);

let canvasThree = document.getElementById('canvas-clock-sec');
let ctxThree = canvas.getContext('2d');

//canvasThree.width = 500;
//canvasThree.height = 500;

function drawSec(delta) {
    requestAnimationFrame(drawSec);
    canvasThree.width = canvas.width;
    ctxThree.fillStyle = "rgba(0, 0, 0, 0.4)";
    
    
    let randomLeft = (Math.abs(Math.pow( Math.sin(delta/(sec * 60)), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/(sec * 60)) + 6), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/(sec * 60))+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/(sec * 60))+1), 2)) * 100;
    
    ctxThree.beginPath();
    ctxThree.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxThree.bezierCurveTo(canvasThree.width / 3, randomLeftConstraint, canvasThree.width / 3 * 2, randomRightConstraint, canvasThree.width, randomRight);


    ctxThree.stroke();
}
requestAnimationFrame(drawSec);




